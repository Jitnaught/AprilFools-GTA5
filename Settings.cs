﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace AprilFools
{
    public class Settings
    {
        public Keys startKey { get; set; }
        public Keys stopKey { get; set; }
        public int foolInterval { get; set; }
        public List<Fool> fools { get; set; }

        public Settings() { }
        public Settings(Keys startKey, Keys stopKey, int foolInterval, List<Fool> fools)
        {
            this.startKey = startKey;
            this.stopKey = stopKey;
            this.foolInterval = foolInterval;
            this.fools = fools;
        }
    }
}
