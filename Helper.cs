﻿using GTA;
using GTA.Native;

namespace AprilFools
{
    class Helper
    {
        internal enum RagdollType
        {
            Normal = 0,
            StiffLegs = 1,
            NarrowLegs = 2,
            WideLegs = 3,
        }

        internal static void RagdollPed(Ped ped, int duration = -1, RagdollType ragdollType = RagdollType.Normal)
        {
            ped.CanRagdoll = true;
            Function.Call(Hash.SET_PED_TO_RAGDOLL, ped.Handle, duration, duration, (int)ragdollType, false, false, false);
        }
    }
}
