﻿using System.Media;
using System.Threading.Tasks;

namespace AprilFools
{
    internal class cSoundPlayer : SoundPlayer
    {
        internal bool IsPlaying;

        internal cSoundPlayer(string soundLocation) : base(soundLocation)
        {
            IsPlaying = false;
        }

        internal void MyPlay()
        {
            IsPlaying = true;

            Task.Factory.StartNew(() =>
            {
                PlaySync();
                IsPlaying = false;
            });
        }

        internal void MyStop()
        {
            Stop();
            IsPlaying = false;
        }
    }
}
