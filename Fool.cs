﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Threading.Tasks;

namespace AprilFools
{
    public class Fool
    {
        public enum FoolType
        {
            ChangeScreenRotation,
            FlyUpwards,
            PlaySound,
            TextToSpeech,
            Scare,
            Slip
        }

        public FoolType foolType;
        public object[] data;
        public int length;

        Random soundRand = new Random();
        Random ttsRand = new Random();

        internal cSoundPlayer player = null;
        SpeechSynthesizer synth = null;
        internal int whenStop = 0;

        public Fool() { }

        public Fool(FoolType foolType, object[] data, int length = 0)
        {
            this.foolType = foolType;
            this.data = data;
            this.length = length;
        }

        internal bool IsValid
        {
            get
            {
                switch (foolType)
                {
                    case FoolType.PlaySound:
                    {
                        return data.All(x => x.GetType() == typeof(string) && File.Exists((string)x) && Path.GetExtension((string)x) == ".wav");
                    }
                    case FoolType.TextToSpeech:
                    {
                        if (data.Length < 2) return false;

                        if (data[0].GetType() != typeof(int)) return false;

                        for (int i = 1; i < data.Length; i++)
                        {
                            if (data[i].GetType() != typeof(string)) return false;
                        }

                        return true;
                    }
                    case FoolType.Scare:
                    {
                        return data.Length == 2 && data.All(x => x.GetType() == typeof(string) && File.Exists((string)x)) && Path.GetExtension((string)data[0]) == ".wav" && Path.GetExtension((string)data[1]) == ".png";
                    }
                }

                return true;
            }
        }

        internal bool IsDrawType
        {
            get
            {
                switch (foolType)
                {
                    case FoolType.Scare:
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        internal void LoadImage()
        {
            switch (foolType)
            {
                case FoolType.Scare:
                {
                    UI.DrawTexture((string)data[1], 0, 0, 1, new Point(0, 0), new Size(0, 0));

                    break;
                }
            }
        }

        internal void DoFool()
        {
            //UI.ShowSubtitle("Doing " + foolType.ToString());

            switch (foolType)
            {
                case FoolType.ChangeScreenRotation:
                {
                    Display.ChangeScreenOrientation(Display.DMDO._180);

                    whenStop = Game.GameTime + length;
                    break;
                }
                case FoolType.FlyUpwards:
                {
                    Ped plrPed = Game.Player.Character;

                    if (plrPed != null && plrPed.Exists() && plrPed.IsAlive)
                    {
                        if (plrPed.IsInVehicle())
                        {
                            Vehicle plrVeh = plrPed.CurrentVehicle;

                            if (plrVeh != null && plrVeh.Exists())
                            {
                                plrVeh.ApplyForce(new Vector3(0f, 0f, 100f), new Vector3(25f, 25f, 25f));
                            }
                        }
                        else
                        {
                            Helper.RagdollPed(plrPed, 3000);
                            plrPed.ApplyForce(new Vector3(0f, 0f, 100f));
                        }

                        Function.Call(Hash.PLAY_PAIN, plrPed, 8, 0, 0);
                    }
                    break;
                }
                case FoolType.PlaySound:
                {
                    string location;

                    if (data.Length == 1)
                    {
                        location = (string)data[0];
                    }
                    else
                    {
                        location = (string)data[soundRand.Next(0, data.Length)];
                    }

                    if (player == null) player = new cSoundPlayer(location);
                    else player.SoundLocation = location;

                    player.MyPlay();
                    break;
                }
                case FoolType.TextToSpeech:
                {
                    Task.Factory.StartNew(new Action(() =>
                    {
                        if (synth == null)
                        {
                            synth = new SpeechSynthesizer();
                            synth.SetOutputToDefaultAudioDevice();
                            synth.Volume = (int)data[0];
                        }

                        string say;

                        if (data.Length == 2)
                        {
                            say = (string)data[1];
                        }
                        else
                        {
                            say = (string)data[ttsRand.Next(1, data.Length)];
                        }

                        synth.Speak(say);
                    }));
                    break;
                }
                case FoolType.Scare:
                {
                    if (player == null) player = new cSoundPlayer((string)data[0]);
                    else player.SoundLocation = (string)data[0];

                    player.MyPlay();
                    break;
                }
                case FoolType.Slip:
                {
                    Ped plrPed = Game.Player.Character;

                    if (plrPed != null && plrPed.Exists() && plrPed.IsAlive)
                    {
                        if (plrPed.IsInVehicle())
                        {
                            Script.Wait(1000);
                            AprilFools.DoRandFool();
                            return;
                        }

                        Vector3 pos = plrPed.ForwardVector;
                        plrPed.CanRagdoll = true;
                        Function.Call(Hash.SET_PED_TO_RAGDOLL_WITH_FALL, plrPed, 2000, 0, (int)Helper.RagdollType.StiffLegs, pos.X, pos.Y, pos.Z, 100f, 0f, 0f, 0f, 0f, 0f, 0f);
                    }
                    break;
                }
            }
        }

        internal void StopFool()
        {
            switch (foolType)
            {
                case FoolType.ChangeScreenRotation:
                {
                    Display.ChangeScreenOrientation(Display.DMDO.DEFAULT);
                    break;
                }
                case FoolType.TextToSpeech:
                case FoolType.Scare:
                {
                    if (player != null)
                    {
                        player.MyStop();
                        player = null;
                    }
                    break;
                }
            }

            whenStop = 0;
        }
    }
}
