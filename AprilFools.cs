﻿using GTA;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace AprilFools
{
    public class AprilFools : Script
    {
        static Settings settings;

        bool enabled = false;
        double lastTickTime = 0, lastFoolTime = 0;
        static Random rand = new Random();
        static Fool lastFool = null;

        string errorMessage;

        public AprilFools()
        {
            string xmlLocation = Path.ChangeExtension(Filename, ".xml");

            if (File.Exists(xmlLocation))
            {
                XmlReader reader = null;

                try
                {
                    reader = XmlReader.Create(xmlLocation);
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));

                    settings = (Settings)serializer.Deserialize(reader);
                    reader.Close();
                }
                catch (Exception)
                {
                    if (reader != null) reader.Close();
                    ShowErrorWhenInGame("Error loading fools settings. Make sure file is of correct format.");
                    return;
                }

                if (settings.fools.Count != 0)
                {
                    bool allAdded = true;

                    for (int i = 0; i < settings.fools.Count; i++)
                    {
                        Fool fool = settings.fools[i];

                        if (!fool.IsValid || settings.fools.Count(x => x.foolType == fool.foolType) > 1)
                        {
                            settings.fools.RemoveAt(i);
                            i--;
                            allAdded = false;
                        }
                        else if (fool.IsDrawType)
                        {
                            fool.LoadImage();
                        }
                    }

                    if (settings.fools.Count != 0)
                    {
                        if (!allAdded) ShowErrorWhenInGame("Some fools not loaded, as they were either not valid or were duplicates.");
                        
                        KeyUp += AprilFools_KeyUp;
                        Interval = 0;
                    }
                    else
                    {
                        ShowErrorWhenInGame("No fools loaded. Make sure they are valid.");
                    }
                }
                else
                {
                    ShowErrorWhenInGame("No fools loaded. Make sure you have added some.");
                }
            }
            else ShowErrorWhenInGame("Error loading fools settings. The settings file does not exist.");
        }

        public static void DoRandFool()
        {
            Fool fool;

            if (settings.fools.Count == 1) (fool = settings.fools[0]).DoFool();
            else
            {
                //don't want to get the same fool again
                while ((fool = settings.fools[rand.Next(0, settings.fools.Count)]) == lastFool && settings.fools.Count > 2) Yield();

                fool.DoFool();
            }

            lastFool = fool;
        }

        void ShowErrorWhenInGame(string message)
        {
            errorMessage = message;
            Tick += Error_Tick;
        }

        void Error_Tick(object sender, EventArgs e)
        {
            Ped plrPed;

            if (!Game.IsLoading && !Game.IsScreenFadedOut && !Game.IsScreenFadingIn
                && (plrPed = Game.Player.Character) != null && plrPed.Exists() && plrPed.IsAlive)
            {
                UI.Notify(errorMessage);
                Tick -= Error_Tick;
            }
        }

        void AprilFools_Tick(object sender, EventArgs e)
        {
            foreach (Fool fool in settings.fools)
            {
                if (fool.IsDrawType && ((fool.whenStop != 0 && Game.GameTime < fool.whenStop) || (fool.player != null && fool.player.IsPlaying)))
                {
                    UI.DrawTexture((string)fool.data[1], 0, 0, 200, new Point(0, 0), Game.ScreenResolution);
                    break;
                }

                Yield();
            }

            int now = Game.GameTime;

            if (now > lastTickTime + 500)
            {
                if (now > lastFoolTime + settings.foolInterval)
                {
                    DoRandFool();
                    
                    lastFoolTime = now;
                }

                foreach (Fool fool in settings.fools)
                {
                    if (fool.whenStop != 0 && now > fool.whenStop)
                    {
                        fool.StopFool();
                    }

                    Yield();
                }

                lastTickTime = now;
            }
        }

        void AprilFools_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == settings.startKey && !enabled)
            {
                enabled = true;

                lastFoolTime = Game.GameTime;

                Tick += AprilFools_Tick;
            }

            if (e.KeyCode == settings.stopKey && enabled)
            {
                enabled = false;

                Tick -= AprilFools_Tick;

                foreach (Fool fool in settings.fools)
                {
                    fool.StopFool();
                }
            }
        }
    }
}
