﻿using System.Runtime.InteropServices;

namespace AprilFools
{
    internal class Display
    {
        internal static DMDO currentOrientation = DMDO.DEFAULT;

        //https://msdn.microsoft.com/en-us/library/ms812499.aspx
        #region Enums, structs, and constants
        internal enum DMDO : int
        {
            DEFAULT,
            _90,
            _180,
            _270
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        struct DEVMODE
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            internal string dmDeviceName;

            short dmSpecVersion;
            short dmDriverVersion;
            internal short dmSize;
            short dmDriverExtra;
            int dmFields;
            int dmPositionX;
            int dmPositionY;
            internal DMDO dmDisplayOrientation;
            int dmDisplayFixedOutput;
            short dmColor;
            short dmDuplex;
            short dmYResolution;
            short dmTTOption;
            short dmCollate;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            internal string dmFormName;

            short dmLogPixels;
            short dmBitsPerPel;
            internal int dmPelsWidth;
            internal int dmPelsHeight;
            int dmDisplayFlags;
            int dmDisplayFrequency;
            int dmICMMethod;
            int dmICMIntent;
            int dmMediaType;
            int dmDitherType;
            int dmReserved1;
            int dmReserved2;
            int dmPanningWidth;
            int dmPanningHeight;
        };

        enum DISP_CHANGE : int
        {
            Successful = 0,
            Restart = 1,
            Failed = -1,
            BadMode = -2,
            NotUpdated = -3,
            BadFlags = -4,
            BadParam = -5,
            BadDualView = -6
        }
        
        enum ChangeDisplaySettingsFlags : uint
        {
            CDS_NONE = 0,
            CDS_UPDATEREGISTRY = 0x00000001,
            CDS_TEST = 0x00000002,
            CDS_FULLSCREEN = 0x00000004,
            CDS_GLOBAL = 0x00000008,
            CDS_SET_PRIMARY = 0x00000010,
            CDS_VIDEOPARAMETERS = 0x00000020,
            CDS_ENABLE_UNSAFE_MODES = 0x00000100,
            CDS_DISABLE_UNSAFE_MODES = 0x00000200,
            CDS_RESET = 0x40000000,
            CDS_RESET_EX = 0x20000000,
            CDS_NORESET = 0x10000000
        }

        const int ENUM_CURRENT_SETTINGS = -1;
        #endregion  

        [DllImport("user32.dll")]
        static extern DISP_CHANGE ChangeDisplaySettings(ref DEVMODE devMode, ChangeDisplaySettingsFlags flags);

        [DllImport("user32.dll")]
        static extern int EnumDisplaySettings(string deviceName, int modeNum, ref DEVMODE devMode);

        static DEVMODE CreateDEVMODE()
        {
            DEVMODE dm = new DEVMODE();
            dm.dmDeviceName = new string(new char[32]);
            dm.dmFormName = new string(new char[32]);
            dm.dmSize = (short)Marshal.SizeOf(dm);

            return dm;
        }

        internal static bool ChangeScreenOrientation(DMDO orientation)
        {
            DEVMODE devmode = CreateDEVMODE();

            return EnumDisplaySettings(null, ENUM_CURRENT_SETTINGS, ref devmode) != 0 && ChangeScreenOrientation(devmode, orientation);
        }

        static bool ChangeScreenOrientation(DEVMODE devmode, DMDO orientation)
        {
            devmode.dmDisplayOrientation = orientation;

            DISP_CHANGE error = ChangeDisplaySettings(ref devmode, ChangeDisplaySettingsFlags.CDS_NONE);

            if (error == DISP_CHANGE.Successful)
            {
                currentOrientation = devmode.dmDisplayOrientation;

                return true;
            }

            return false;
        }
    }
}
